import requests
from datetime import datetime
import os
import csv

from venturedevs_python_hahaton.settings import FORECASTS_DIR, API_URL, LOCATION_QUERY, JSON_FIELDS


class ServiceException(Exception):
    pass


def get_forecasts_from_api(woeid):
    url = API_URL + "/" + LOCATION_QUERY + "/%d" % woeid
    response = requests.get(url)
    if response.status_code == requests.codes.not_found:
        return None
    return response.json()


def get_header(forecast):
    return list(forecast[JSON_FIELDS.weather][0].keys())


def forecast_to_list(forecast):
    return [list(weather_item.values()) for weather_item in forecast[JSON_FIELDS.weather]]


def get_forecast_file_path(woeid):
    date, time = datetime.now().strftime("%d-%m-%Y %H-%M-%S").split()
    path = os.path.join(FORECASTS_DIR, str(woeid), date)
    filename = "%s.csv" % time
    return path, filename


def save_forecast(forecast, dir, file_name):
    header = get_header(forecast)
    forecast_list = forecast_to_list(forecast)
    if not os.path.exists(dir):
        os.makedirs(dir)

    try:
        with open(os.path.join(dir, file_name), "w") as file:
            csv_writer = csv.writer(file, delimiter='\t')
            csv_writer.writerow(header)
            csv_writer.writerows(forecast_list)
    except (OSError, IOError):
        raise(ServiceException("Problem with a file"))
    except csv.Error:
        raise(ServiceException("Problem with saving to a csv file"))
