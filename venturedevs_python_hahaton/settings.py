FORECASTS_DIR = 'forecasts'
API_URL = 'https://www.metaweather.com/api'
LOCATION_QUERY = 'location'


class JSON_FIELDS:
    status = 'updated'
    weather = 'consolidated_weather'
