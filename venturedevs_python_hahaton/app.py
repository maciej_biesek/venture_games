from flask import Flask, jsonify, request
from flask_api import status

from venturedevs_python_hahaton.service import (get_forecasts_from_api, get_forecast_file_path, save_forecast,
                                                ServiceException)
from venturedevs_python_hahaton.settings import JSON_FIELDS


APP = Flask(__name__)


@APP.route('/update-forecast/<int:woe_id>', methods=['POST'])
def update_forecast(woe_id):
    if request.method == 'POST':
        try:
            forecast = get_forecasts_from_api(woe_id)
            if forecast is not None:
                directory, filename = get_forecast_file_path(woe_id)
                save_forecast(forecast, directory, filename)
                return jsonify({JSON_FIELDS.status: "ok"}), status.HTTP_201_CREATED

            return jsonify({}), status.HTTP_404_NOT_FOUND

        except ServiceException as e:
            print(e)
            return jsonify({}), status.HTTP_500_INTERNAL_SERVER_ERROR
        except Exception as e:
            print(e)
            return jsonify({}), status.HTTP_502_BAD_GATEWAY

    return jsonify({}), status.HTTP_405_METHOD_NOT_ALLOWED
